import React from "react";
import styled from "styled-components";

const ResultsWrapper = styled.div`
  flex: 1 1 auto;
  padding-top: 48px;
  justify-content: center;
  margin: 0 -8px;
  display: flex;
  flex-wrap: wrap;
`;

const EligibilityResults = (props: any) => {
  
  const  elligibleCards = props.eligibilityData.eligibleCards;

  const returnCardNames = () => {

    let cardNames: string[] = [];

    elligibleCards?.forEach((card: string) => {
      if(card === 'C1') {
        cardNames.push('Card One');
      } else if (card === 'C2') {
        cardNames.push('Card Two');
      }
    });

    return cardNames;
  
  },
  canHaveCards = () => {

    const cards = returnCardNames();
    let elligibleCardContent: string;

    // build scentence 
    if(cards.length === 2) {
      elligibleCardContent = `${cards[0]} and ${cards[1]}`;
    } else {
      elligibleCardContent = `${cards[0]}`;
    }
    if(cards.length > 0) {

      return (
        <div className="elligible-content">
          <h1>Congratulations!</h1>
          <h2>You are elligible for {elligibleCardContent}</h2>
        </div>
      )
    } else {
      return (
        <div className="elligible-content">
          <h1>Sorry!</h1>
          <h2>You are not elligible for our cards.</h2>
        </div>
      )
    }
    
  }

  return <ResultsWrapper>
    {canHaveCards()}
  </ResultsWrapper>;
};

export default EligibilityResults;
