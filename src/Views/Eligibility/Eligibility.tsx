import React, { useState } from "react";
import View from "../../DesignSystem/View";
import EligibilityApplication from "./EligibilityApplication";
import EligibilityResults from "./EligibilityResults";

class Eligibility extends React.Component{

  state = {
    data: {},
   }

  childToParent = (responseData:any) => {
    this.setState({data: responseData});
    console.log('parent  ',responseData);
  }

  render(){
    return(
      <View>
        <EligibilityApplication childToParent={this.childToParent} />
        <EligibilityResults eligibilityData={this.state.data} />
    </View>
      )
  }

}

export default Eligibility;
