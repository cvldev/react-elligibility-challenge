import React, { useState } from "react";
import { useFormik } from "formik";
import styled from "styled-components";

import FormInput from "../../../DesignSystem/Form/FormInput";
import SubmitButton from "../../../DesignSystem/Form/SubmitButton";
import Title from "../../../DesignSystem/Title";

const FormWrapper = styled.div`
  flex: 1 1 auto;
  width: 100%;
`;

interface FormValues {
  name: string;
  email: string;
  address: string;
}

const EligibilityApplication = (props:any) => {

  let [eligibility, setEligibility] = useState({eligibleCards:[]});

  const { handleChange, handleSubmit, values } = useFormik<FormValues>({
    initialValues: {
      name: "",
      email: "",
      address: "",
    },
    onSubmit: (values) => {

      // options for post
      const requestOptions = {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify( values )
      };
      
      // send values to backend service
      // Boris C1
      // Angela C1 C2
      // Theresa C2
      // John NONE
      fetch('/eligibility/check', requestOptions).then(response => response.json())
      .then(data => {
        console.log('data ',data);
        setEligibility(eligibility = data);
        props.childToParent(data);
      }).catch(error => {
        console.error('There was an error!', error);
      })
    },
  });
  return (
    <FormWrapper>
      {eligibility.eligibleCards[0]}
      <Title>Cards</Title>
      <form onSubmit={handleSubmit}>
        <FormInput
          type="text"
          name="name"
          id="name"
          onChange={handleChange}
          value={values.name}
          placeholder="Name"
        />
        <FormInput
          type="email"
          name="email"
          id="email"
          onChange={handleChange}
          value={values.email}
          placeholder="Email"
        />
        <FormInput
          type="text"
          name="address"
          id="address"
          onChange={handleChange}
          value={values.address}
          placeholder="Address"
        />
        <SubmitButton text="Submit" />
      </form>
    </FormWrapper>
  );
};

export default EligibilityApplication;
